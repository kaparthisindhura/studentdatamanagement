import java.util.HashMap;
import java.util.Scanner;
import static java.lang.Character.compare;

public class SchoolManagement {
    static HashMap<Integer,String> studentDetails=new HashMap<>();

    static void admin()
    {
        Scanner sc = new Scanner(System.in);
        int input,rollNo;
        String name;
        String[] student;
        char yesOrNo;
        do {

            System.out.println("1.Add Student");
            System.out.println("2.Remove Student");
            System.out.println("3.Update Student data");
            input=Integer.parseInt(sc.nextLine());
            switch(input)
            {
                case 1: System.out.print("Enter Roll No and Name : ");
                    student=sc.nextLine().split(" ");
                    rollNo=Integer.parseInt(student[0]);
                    name=student[1];
                    studentDetails.put(rollNo,name);
                    staff();
                    break;

                case 2: System.out.print("Enter Roll No of student to remove  : ");
                    rollNo=sc.nextInt();
                    studentDetails.remove(rollNo);
                    break;

                case 3: System.out.print("Enter Roll No of student to update  : ");
                    rollNo=Integer.parseInt(sc.nextLine());
                    System.out.print("Enter updated name  : ");
                    name=sc.nextLine();
                    studentDetails.replace(rollNo,name);
                    staff();
                    break;

                default: System.out.println("Enter Correct number");
            }

            System.out.print("Do you want to continue(Y/N) : ");
            yesOrNo=sc.next().charAt(0);
            sc.nextLine();

        }while(compare(yesOrNo,'Y')==0);
    }

    static void staff()
    {
        System.out.println("---Student Data---");
        for(int rollNo :studentDetails.keySet())
        {
            System.out.println("Roll No : "+rollNo+"  Name : " +studentDetails.get(rollNo));
        }


    }


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int input;
        String password;
        char yesOrNo;
        do{
            System.out.println("1.Admin\n2.Staff");
            input=sc.nextInt();
            switch(input)
            {
                case 1: System.out.print("Enter Password : ");
                        password=sc.next();
                        if (password.equals("1234")) {
                            admin();
                        }
                        else {
                            System.out.println("Incorrect Password");
                        }
                        break;


                case 2: staff();
                        break;
                default: System.out.println("Enter Correct number");
            }

            System.out.print("Do you want to continue choosing admin or staff(Y/N) : ");
            yesOrNo=sc.next().charAt(0);

        }while(compare(yesOrNo,'Y')==0);

    }
}
